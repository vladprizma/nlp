import opennlp.tools.parser.Parse;
import opennlp.tools.util.Span;
import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.*;

public class NaturalLanguageProcessingTest {
    //String text = " Divide. For? Sentences! With? OpenNLP! ";
    //text = "This shop is located in Hyderabad";
    //text = "John has a sister named Penny";
    //text = "Internet is the largest tutorial library.";
    //text = "Hi welcome to testing";

    @Test
    public void simpleTextProcessingTest(){
        String text = "Divide. For? Sentences! With? OpenNLP!";
        String[] sentences = { "Divide", " For", " Sentences", " With", " OpenNLP"};
        assertArrayEquals(sentences, NaturalLanguageProcessing.simpleTextProcessing(text));

        text = "Divide...";
        String[] sentences1 = {"Divide"};
        assertArrayEquals(sentences1, NaturalLanguageProcessing.simpleTextProcessing(text));

        text = "";
        String[] sentences2 = {""};
        assertArrayEquals(sentences2, NaturalLanguageProcessing.simpleTextProcessing(text));

        text = ".?!";
        String[] sentences3 = new String[0];
        assertArrayEquals(sentences3, NaturalLanguageProcessing.simpleTextProcessing(text));
    }

    @Test
    public void textSentencesDetectTest(){
        String text = "Divide. For? Sentences! With? OpenNLP!";
        String[] sentences = { "Divide.", "For? Sentences!", "With?", "OpenNLP!"};
        assertArrayEquals(sentences, NaturalLanguageProcessing.textSentencesDetect(text));

        text = "Divide...";
        String[] sentences1 = {"Divide..."};
        assertArrayEquals(sentences1, NaturalLanguageProcessing.textSentencesDetect(text));

        text = "";
        String[] sentences2 = new String[0];
        assertArrayEquals(sentences2, NaturalLanguageProcessing.textSentencesDetect(text));

        text = ".?!";
        String[] sentences3 = {".?!"};
        assertArrayEquals(sentences3, NaturalLanguageProcessing.textSentencesDetect(text));
    }

    @Test
    public void sentencesPositionsInTextTest(){
        String text = "Divide. For? Sentences! With? OpenNLP!";
        Span[] spans = {new Span(0, 7), new Span(8,23), new Span(24,29), new Span(30,38)};
        assertArrayEquals(spans, NaturalLanguageProcessing.sentencesPositionsInText(text));

        text = "Divide...";
        Span[] spans1 = {new Span(0, 9)};
        assertArrayEquals(spans1, NaturalLanguageProcessing.sentencesPositionsInText(text));

        text = "";
        Span[] spans2 = {};
        assertArrayEquals(spans2, NaturalLanguageProcessing.sentencesPositionsInText(text));

        text = ".?!";
        Span[] spans3 = {new Span(0, 3)};
        assertArrayEquals(spans3, NaturalLanguageProcessing.sentencesPositionsInText(text));
    }

    @Test
    public void sentencesProbabilitiesInTextTest(){
        String text = "Divide. For? Sentences! With? OpenNLP!";
        System.out.println(Arrays.toString(NaturalLanguageProcessing.sentencesProbabilitiesInText(text)));
    }

    @Test
    public void textTokenizerTest(){
        String text = "Divide. For? Sentences! With? OpenNLP!";
        String[] sentences = { "Divide", ".", "For", "?", "Sentences", "!", "With", "?", "OpenNLP", "!"};
        assertArrayEquals(sentences, NaturalLanguageProcessing.textTokenizer(text));

        text = "Divide...";
        String[] sentences1 = {"Divide", "..."};
        assertArrayEquals(sentences1, NaturalLanguageProcessing.textTokenizer(text));

        text = "";
        String[] sentences2 = new String[0];
        assertArrayEquals(sentences2, NaturalLanguageProcessing.textTokenizer(text));

        text = ".?!";
        String[] sentences3 = {".", "?", "!"};
        assertArrayEquals(sentences3, NaturalLanguageProcessing.textTokenizer(text));
    }

    @Test
    public void textWhitespaceTokenizerTest(){
        String text = "Divide. For? Sentences! With? OpenNLP!";
        String[] sentences = { "Divide.", "For?", "Sentences!", "With?", "OpenNLP!"};
        assertArrayEquals(sentences, NaturalLanguageProcessing.textWhitespaceTokenizer(text));

        text = "Divide...";
        String[] sentences1 = {"Divide..."};
        assertArrayEquals(sentences1, NaturalLanguageProcessing.textWhitespaceTokenizer(text));

        text = "";
        String[] sentences2 = new String[0];
        assertArrayEquals(sentences2, NaturalLanguageProcessing.textWhitespaceTokenizer(text));

        text = ".?!";
        String[] sentences3 = {".?!"};
        assertArrayEquals(sentences3, NaturalLanguageProcessing.textWhitespaceTokenizer(text));
    }

    @Test
    public void textTokenizerModelTest(){
        String text = "Divide. For? Sentences! With? OpenNLP!";
        String[] sentences = { "Divide", ".", "For", "?", "Sentences", "!", "With", "?", "OpenNLP", "!"};
        assertArrayEquals(sentences, NaturalLanguageProcessing.textTokenizerModel(text));

        text = "Divide...";
        String[] sentences1 = {"Divide", "..."};
        assertArrayEquals(sentences1, NaturalLanguageProcessing.textTokenizerModel(text));

        text = "";
        String[] sentences2 = new String[0];
        assertArrayEquals(sentences2, NaturalLanguageProcessing.textTokenizerModel(text));

        text = ".?!";
        String[] sentences3 = {".", "?", "!"};
        assertArrayEquals(sentences3, NaturalLanguageProcessing.textTokenizerModel(text));
    }

    @Test
    public void tokensPositionsTest(){
        String text = "Divide. For? Sentences! With? OpenNLP!";
        Span[] spans = {new Span(0, 6), new Span(6,7), new Span(8,11), new Span(11,12),
                new Span(13, 22), new Span(22,23), new Span(24,28), new Span(28,29),
                new Span(30, 37), new Span(37,38)};
        assertArrayEquals(spans, NaturalLanguageProcessing.tokensPositions(text));

        text = "Divide...";
        Span[] spans1 = {new Span(0, 6), new Span(6, 9)};
        assertArrayEquals(spans1, NaturalLanguageProcessing.tokensPositions(text));

        text = "";
        Span[] spans2 = {};
        assertArrayEquals(spans2, NaturalLanguageProcessing.tokensPositions(text));

        text = ".?!";
        Span[] spans3 = {new Span(0, 1), new Span(1, 2), new Span(2, 3)};
        assertArrayEquals(spans3, NaturalLanguageProcessing.tokensPositions(text));
    }

    @Test
    public void tokensProbabilitiesInTextTest(){
        String text = "Divide. For? Sentences! With? OpenNLP!";
        System.out.println(Arrays.toString(NaturalLanguageProcessing.tokensProbabilitiesInText(text)));
    }

    @Test
    public void namesInStringsTest(){
        String text = "John has a sister named Penny";
        String[] sentences = NaturalLanguageProcessing.textTokenizerModel(text);
        assertArrayEquals(new Span[]{new Span(0, 1, "person"), new Span(5, 6, "person")}, NaturalLanguageProcessing.namesInStrings(sentences));

        text = "John you did this?";
        String[] sentences1 = NaturalLanguageProcessing.textTokenizerModel(text);
        assertArrayEquals(new Span[]{new Span(0, 1, "person")}, NaturalLanguageProcessing.namesInStrings(sentences1));

        text = "";
        String[] sentences2 = NaturalLanguageProcessing.textTokenizerModel(text);
        assertArrayEquals(new Span[]{}, NaturalLanguageProcessing.namesInStrings(sentences2));

        text = ".?!";
        String[] sentences3 = NaturalLanguageProcessing.textTokenizerModel(text);
        assertArrayEquals(new Span[]{}, NaturalLanguageProcessing.namesInStrings(sentences3));
    }

    @Test
    public void locationsInStringsTest(){
        String text = "This shop is located in Hyderabad";
        String[] sentences = NaturalLanguageProcessing.textTokenizerModel(text);
        assertArrayEquals(new Span[]{new Span(5, 6, "location")}, NaturalLanguageProcessing.locationsInStrings(sentences));

        text = "You've been in New York?";
        String[] sentences1 = NaturalLanguageProcessing.textTokenizerModel(text);
        assertArrayEquals(new Span[]{new Span(4, 6, "location")}, NaturalLanguageProcessing.locationsInStrings(sentences1));

        text = "";
        String[] sentences2 = NaturalLanguageProcessing.textTokenizerModel(text);
        assertArrayEquals(new Span[]{}, NaturalLanguageProcessing.locationsInStrings(sentences2));

        text = ".?!";
        String[] sentences3 = NaturalLanguageProcessing.textTokenizerModel(text);
        assertArrayEquals(new Span[]{}, NaturalLanguageProcessing.locationsInStrings(sentences3));
    }

    @Test
    public void partOfSpeechInTextTest(){
        String text = "Divide for sentences with OpenNLP";
        String expected = "Divide_VB for_IN sentences_NNS with_IN OpenNLP_NNP";
        assertEquals(expected, NaturalLanguageProcessing.partOfSpeechInText(text));

        text = "Divide";
        expected = "Divide_VB";
        assertEquals(expected, NaturalLanguageProcessing.partOfSpeechInText(text));

        text = "";
        expected = "";
        assertEquals(expected, NaturalLanguageProcessing.partOfSpeechInText(text));

        // _. - это пунктуация
        text = ".?!";
        expected = ".?!_.";
        assertEquals(expected, NaturalLanguageProcessing.partOfSpeechInText(text));
    }

    @Test
    public void textParsingTest(){
        String text = "Divide. For? Sentences! With? OpenNLP!";
        String expected = "(TOP (NP (NNP Divide.) (NNP For?) (NNP Sentences!) (NNP With?) (NNP OpenNLP!)))";
        StringBuffer actual = new StringBuffer();
        for(Parse parse : NaturalLanguageProcessing.textParsing(text)) {
            parse.show(actual);
        }
        assertEquals(expected, actual.toString());
    }

    @Test
    public void textChunkingTest(){
        String text = "Hi welcome to testing";
        String[] expected = {"B-NP", "I-NP", "B-VP", "I-VP"};
        String[] actual = NaturalLanguageProcessing.textChunking(text);
        assertArrayEquals(expected, actual);
    }

}