import opennlp.tools.chunker.ChunkerME;
import opennlp.tools.chunker.ChunkerModel;
import opennlp.tools.cmdline.parser.ParserTool;
import opennlp.tools.cmdline.postag.POSModelLoader;
import opennlp.tools.namefind.NameFinderME;
import opennlp.tools.namefind.TokenNameFinderModel;
import opennlp.tools.parser.Parse;
import opennlp.tools.parser.Parser;
import opennlp.tools.parser.ParserFactory;
import opennlp.tools.parser.ParserModel;
import opennlp.tools.postag.POSModel;
import opennlp.tools.postag.POSSample;
import opennlp.tools.postag.POSTaggerME;
import opennlp.tools.sentdetect.SentenceDetectorME;
import opennlp.tools.sentdetect.SentenceModel;
import opennlp.tools.tokenize.SimpleTokenizer;
import opennlp.tools.tokenize.TokenizerME;
import opennlp.tools.tokenize.TokenizerModel;
import opennlp.tools.tokenize.WhitespaceTokenizer;
import opennlp.tools.util.Span;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

public class NaturalLanguageProcessing {

    // Простое разделение текста на предложения
    public static String[] simpleTextProcessing(String text){
        String simple = "[.?!]";
        return (text.split(simple));
    }

    // Обнаружение предложений с помощью модели en-sent.bin
    public static String[] textSentencesDetect(String text){
        String[] sentences = new String[0];
        try(InputStream inputStream = new FileInputStream("C:\\Users\\glitc\\Downloads\\Thumbtack\\NLP\\models\\en-sent.bin")) {
            SentenceModel model = new SentenceModel(inputStream);
            SentenceDetectorME detector = new SentenceDetectorME(model);
            sentences = detector.sentDetect(text);
        }
        catch (IOException e){
            e.printStackTrace();
        }
        return sentences;
    }

    // Определение интервалов предложений
    public static Span[] sentencesPositionsInText(String text) {
        Span[] spans = new Span[0];
        try(InputStream inputStream = new FileInputStream("C:\\Users\\glitc\\Downloads\\Thumbtack\\NLP\\models\\en-sent.bin")) {
            SentenceModel model = new SentenceModel(inputStream);
            SentenceDetectorME detector = new SentenceDetectorME(model);
            spans = detector.sentPosDetect(text);
        }
        catch (IOException e){
            e.printStackTrace();
        }
        return spans;
    }

    // Вероятность предложений
    public static double[] sentencesProbabilitiesInText(String text) {
        String[] sentences = new String[0];
        double[] probabilities = new double[0];
        try(InputStream inputStream = new FileInputStream("C:\\Users\\glitc\\Downloads\\Thumbtack\\NLP\\models\\en-sent.bin")) {
            SentenceModel model = new SentenceModel(inputStream);
            SentenceDetectorME detector = new SentenceDetectorME(model);
            sentences = detector.sentDetect(text);
            probabilities = detector.getSentenceProbabilities();
        }
        catch (IOException e){
            e.printStackTrace();
        }
        return probabilities;
    }

    // Простой токенайзер
    public static String[] textTokenizer(String text) {
        SimpleTokenizer simpleTokenizer = SimpleTokenizer.INSTANCE;
        return simpleTokenizer.tokenize(text);
    }

    // Токенайзер по пробелам
    public static String[] textWhitespaceTokenizer(String text) {
        WhitespaceTokenizer whitespaceTokenizer = WhitespaceTokenizer.INSTANCE;
        return whitespaceTokenizer.tokenize(text);
    }

    // Модель-токенайзер
    public static String[] textTokenizerModel(String text) {
        String[] tokens = new String[0];
        try(InputStream inputStream = new FileInputStream("C:\\Users\\glitc\\Downloads\\Thumbtack\\NLP\\models\\en-token.bin")) {
            TokenizerModel tokenModel = new TokenizerModel(inputStream);
            TokenizerME tokenizer = new TokenizerME(tokenModel);
            tokens = tokenizer.tokenize(text);
        }
        catch (IOException e){
            e.printStackTrace();
        }
        return tokens;
    }

    // Определение интервалов токенов
    public static Span[] tokensPositions(String text) {
        SimpleTokenizer simpleTokenizer = SimpleTokenizer.INSTANCE;
        return simpleTokenizer.tokenizePos(text);
    }

    // Вероятности токенов в тексте
    public static double[] tokensProbabilitiesInText(String text) {
        double[] probabilities = new double[0];
        try (InputStream inputStream = new FileInputStream("C:\\Users\\glitc\\Downloads\\Thumbtack\\NLP\\models\\en-token.bin")) {
            TokenizerModel tokenModel = new TokenizerModel(inputStream);
            TokenizerME tokenizer = new TokenizerME(tokenModel);
            Span[] tokens = tokenizer.tokenizePos(text);
            probabilities = tokenizer.getTokenProbabilities();
        }
        catch(IOException e){
            e.printStackTrace();
        }
        return probabilities;
    }

    // Нахождение имен в массиве строк
    public static Span[] namesInStrings(String[] sentence) {
        Span[] nameSpans = new Span[0];
        try(InputStream inputStream = new FileInputStream("C:\\Users\\glitc\\Downloads\\Thumbtack\\NLP\\models\\en-ner-person.bin")) {
            TokenNameFinderModel model = new TokenNameFinderModel(inputStream);
            NameFinderME nameFinder = new NameFinderME(model);
            nameSpans = nameFinder.find(sentence);
        }
        catch(IOException e){
            e.printStackTrace();
        }
        return nameSpans;
    }

    // Нахождение локаций в токенизированном тексте
    public static Span[] locationsInStrings(String[] tokens) {
        Span[] nameSpans = new Span[0];
        try(InputStream inputStream = new FileInputStream("C:\\Users\\glitc\\Downloads\\Thumbtack\\NLP\\models\\en-ner-location.bin")) {
            TokenNameFinderModel model = new TokenNameFinderModel(inputStream);
            NameFinderME nameFinder = new NameFinderME(model);
            nameSpans = nameFinder.find(tokens);

        }
        catch(IOException e){
            e.printStackTrace();
        }
        return nameSpans;
    }

    // Нахождение частей речи слов в строке
    public static String partOfSpeechInText(String text) {
        String result = null;
        try(InputStream inputStream = new FileInputStream("C:\\Users\\glitc\\Downloads\\Thumbtack\\NLP\\models\\en-pos-maxent.bin")){
            POSModel model = new POSModel(inputStream);
            POSTaggerME tagger = new POSTaggerME(model);

            WhitespaceTokenizer whitespaceTokenizer = WhitespaceTokenizer.INSTANCE;
            String[] tokens = whitespaceTokenizer.tokenize(text);
            String[] tags = tagger.tag(tokens);
            POSSample sample = new POSSample(tokens, tags);
            result = sample.toString();
        }
        catch(IOException e){
            e.printStackTrace();
        }
        return result;
    }

    // Разбор предложения
    public static Parse[] textParsing(String text) {
        Parse[] topParses = new Parse[0];
        try(InputStream inputStream = new FileInputStream("C:\\Users\\glitc\\Downloads\\Thumbtack\\NLP\\models\\en-parser-chunking.bin")){
            ParserModel model = new ParserModel(inputStream);
            Parser parser = ParserFactory.create(model);
            topParses = ParserTool.parseLine(text, parser, 1);
        }
        catch(IOException e){
            e.printStackTrace();
        }
        return topParses;
    }

    // Чанкинг предложения (разделение предложения на части слов, такие как группы слов и группы глаголов)
    public static String[] textChunking(String text) {
        WhitespaceTokenizer whitespaceTokenizer= WhitespaceTokenizer.INSTANCE;
        String[] tokens = whitespaceTokenizer.tokenize(text);
        File file = new File("C:\\Users\\glitc\\Downloads\\Thumbtack\\NLP\\models\\en-pos-maxent.bin");
        POSModel model = new POSModelLoader().load(file);
        POSTaggerME tagger = new POSTaggerME(model);
        String[] tags = tagger.tag(tokens);
        String[] result = new String[0];

        try(InputStream inputStream = new FileInputStream("C:\\Users\\glitc\\Downloads\\Thumbtack\\NLP\\models\\en-chunker.bin")) {
            ChunkerModel chunkerModel = new ChunkerModel(inputStream);
            ChunkerME chunkerME = new ChunkerME(chunkerModel);
            result = chunkerME.chunk(tokens, tags);
        }
        catch(IOException e){
            e.printStackTrace();
        }
        return result;
    }

    public static void main(String[] args) {
        String text = " Divide. For? Sentences! With? OpenNLP! ";
        //text = "This shop is located in Hyderabad";
        //text = "John has a sister named Penny";
        //text = "Internet is the largest tutorial library.";
        //text = "Hi welcome to testing";

    }
}

